import { Body, Delete, Example, Get, Patch, Post, Route } from 'tsoa';
import { User, UserCreateRequest, UserUpdateRequest } from '../models/user';

@Route('Users')
export class UsersController {
  public async Current(): Promise<User> {
    return {
      createdAt: new Date(),
      email: 'test',
      id: 666,
    };
  }

  @Get('{userId}')
  public async Get(userId: number): Promise<User> {
    return {
      createdAt: new Date(),
      email: 'test2',
      id: userId,
    };
  }

  @Post()
  public async Create(@Body() request: UserCreateRequest): Promise<User> {
    return {
      createdAt: new Date(),
      email: request.email,
      id: 666,
    };
  }

  @Delete('{userId}')
  public async Delete(userId: number): Promise<void> {
    return Promise.resolve();
  }

  @Patch()
  public async Update(@Body() request: UserUpdateRequest): Promise<User> {
    return {
      createdAt: request.createdAt,
      email: request.email,
      id: 1337,
    };
  }
}
